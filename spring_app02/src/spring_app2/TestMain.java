package spring_app2;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class TestMain{
	public static void main(String[] args) {
		/*
		interface로 변경
		xml로부터 bean 정보를 얻어 factory로 부터 빈 객체를 얻어서 사용
		
		Coffee c = new CoffeeImple(); // 구현객체
		c.dringk("반장");
		*/
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("src/app.xml"));
		
		Object obj = factory.getBean("coffee");
		Coffee cf = (Coffee)obj;
		
		cf.dringk("반장");
		
	}
}
