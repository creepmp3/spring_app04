package spring_app2;

public class CoffeeImple implements Coffee{
	String type;
	
	public CoffeeImple(String type){
		this.type = type;
	}
	
	@Override
	public void dringk(String name){
		System.out.println(name + "님이 " + type + " 을/를 홀짝");
	}
}
